# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    puts "Welcome to the Towers of Hanoi"
    puts "------------------------------"
    puts "Exit with 'e'"
    while true
      render
      from_tower = get_input("Enter your the tower you wish to move from: 1, 2, or 3")
      break unless from_tower

      to_tower = get_input("Enter the tower you wish to move to: 1, 2, or 3")
      break unless to_tower

      unless valid_move?(from_tower, to_tower)
        puts "Not a valid move, try again."
        next
      end

      move(from_tower, to_tower)

      if won?
        puts "Congratulations! You've beat the Towers of Hanoi"
        render
        break
      end

    end

  end

  # Returns false if user inputs 'e' to signal break from play loop.
  def get_input(message)
    puts message
    userinput = gets.chomp
    return false if userinput == 'e'
    userinput - 1
  end

  def render
    puts "Towers: #{@towers}"
  end

  def move(from_tower, to_tower)
    @towers[to_tower].push(@towers[from_tower].pop)
  end

  def valid_move?(from_tower, to_tower)
    return false if @towers[from_tower].empty?
    return true if @towers[to_tower].empty?
    return false if @towers[from_tower][-1] > @towers[to_tower][-1]
    true
  end

  def won?
    return true if @towers[1..2].any? { |tower| tower == [3, 2, 1] }
    false
  end
end

game = TowersOfHanoi.new
game.play
